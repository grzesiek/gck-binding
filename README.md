# gck-binding PoC

This project aims to solve the problem of accessing Ruby binding in a heavily
dockerized development environment.

[extend description]

## How to use it?

- Build `gck-sshd` image:

```bash
$ cd gck-sshd
$ make image
```

- Run the container

```bash
$ docker run -d -p 2222:22 gck-sshd:latest
```

- In a separate terminal run the Ruby binding

```bash
$ ruby gck-binding-pry/run.rb
```

- SSH into the container (password: `gck`)

```bash
$ ssh root@localhost -p 2222
```

- Attach your debugging session

```bash
$ gck-pry attach
```

:tada:
