#!/usr/bin/env ruby

require_relative 'session'
require_relative 'stream'
require_relative 'tunnel'

class Binding
  def gck(args = {})
    GCK::Debug::Tunnel.new(**args).tap do |tunnel|
      GCK::Debug::Session.new(tunnel).spawn(self)
    end
  end
end
