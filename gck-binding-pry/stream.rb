module GCK
  module Debug
    class Input < SimpleDelegator
      def initialize(input, output)
        @input = input
        @output = output

        __setobj__ input
      end

      def readline(prompt)
        @output.print(prompt)

        @input.readline("\n")
      end
    end
  end
end
