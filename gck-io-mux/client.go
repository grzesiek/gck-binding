package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"sync"
)

type Client struct {
	uuid string
}

func NewClient(uuid string) (*Client, error) {
	return &Client{uuid: uuid}, nil
}

func (c *Client) Start() error {
	// 1. Connect to Unix Socket syscall.SOCK_STREAM type
	// 2. Read connection and pipe it to a os.Stdout
	// 3. Read input and write it to a connection

	conn, err := c.connect()
	if err != nil {
		return err
	}
	defer conn.Close()

	fmt.Println("attaching to a debugger session")

	wg := &sync.WaitGroup{}
	wg.Add(2)

	go c.pipeOutput(wg, conn)
	go c.pipeInput(wg, conn)

	wg.Wait()

	return nil
}

func (c *Client) connect() (net.Conn, error) {
	sock := fmt.Sprintf("%s.sock", c.uuid)

	return net.Dial("unix", sock)
}

func (c *Client) pipeOutput(wg *sync.WaitGroup, conn net.Conn) {
	defer wg.Done()

	_, err := io.Copy(os.Stdout, conn)

	if err != nil {
		fmt.Printf("# server disconnected")
	}
}

func (c *Client) pipeInput(wg *sync.WaitGroup, conn net.Conn) {
	defer wg.Done()

	_, err := io.Copy(conn, os.Stdin)

	if err != nil {
		fmt.Printf("# server disconnected")
	}
}
