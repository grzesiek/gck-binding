package main

import (
	"errors"
	"net"
	"sync"
	"sync/atomic"
)

type Conns struct {
	count int32
	conns map[int32]net.Conn
	mux   *sync.RWMutex
}

func NewConnections() *Conns {
	return &Conns{
		conns: make(map[int32]net.Conn),
		mux:   &sync.RWMutex{},
		count: 0}
}

func (f *Conns) Add(conn net.Conn) int32 {
	f.mux.Lock()
	defer f.mux.Unlock()

	id := atomic.AddInt32(&f.count, 1)
	f.conns[id] = conn

	return id
}

func (f *Conns) Remove(id int32) net.Conn {
	f.mux.Lock()
	defer f.mux.Unlock()

	conn := f.conns[id]
	delete(f.conns, id)

	return conn
}

func (f *Conns) Fanout(b []byte) {
	f.mux.RLock()
	defer f.mux.RUnlock()

	for _, conn := range f.conns {
		conn.Write(b)
	}
}

func (f *Conns) FanoutWithout(skipId int32, b []byte) {
	f.mux.RLock()
	defer f.mux.RUnlock()

	for id, conn := range f.conns {
		if id != skipId {
			conn.Write(b)
		}
	}
}

func (f *Conns) Write(id int32, b []byte) (int, error) {
	f.mux.RLock()
	defer f.mux.RUnlock()

	conn := f.conns[id]

	if conn != nil {
		return conn.Write(b)
	}

	return 0, errors.New("connection not found")
}
