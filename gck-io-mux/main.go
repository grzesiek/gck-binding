package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	session, err := newSession(os.Args[1])
	if err != nil {
		log.Fatalf("could not build io mux session: %v", err)
	}

	if err := session.Start(); err != nil {
		log.Fatalf("could not start io mux session: %v", err)
	}
}

func newSession(command string) (Session, error) {
	switch command {
	case "start":
		return NewServer("gck")
	case "attach":
		return NewClient("gck")
	}

	return nil, fmt.Errorf("unknown command %q", command)
}
