package main

import (
	"fmt"
	"io"
	"net"
	"os"
)

type Session interface {
	Start() error
}

type Log struct {
	uuid string
	path string
	file *os.File
}

func NewSessionLog(uuid string) (*Log, error) {
	name := fmt.Sprintf("%s.log", uuid)

	file, err := os.OpenFile(name, os.O_TRUNC|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}

	return &Log{uuid: uuid, path: name, file: file}, nil
}

func (f *Log) Write(b []byte) (int, error) {
	return f.file.Write(b)
}

func (f *Log) Rewind(conn net.Conn) {
	log, err := os.Open(f.path)
	if err != nil {
		return
	}

	defer log.Close()

	io.Copy(conn, log)
}
